//
//  SmiSdkHelper.swift
//  CapacitorSmisdkPlugin
//
//  Created by Damandeep Singh on 09/07/20.
//

import UIKit
import SmiSdk

@objc(SmiSdkHelper)
public class SmiSdkHelper: NSObject {
    public static let shared = SmiSdkHelper()
    var initalSmiResult:SmiResult?
    
    private override init() {
        super.init()
    }
    
    @objc public func startListener() {
        NotificationCenter.default.addObserver(self, selector: #selector(stateChange(notification:)), name: NSNotification.Name(rawValue: SDSTATE_CHANGE_NOTIF), object: nil)
    }
    
    @objc private func stateChange(notification : Notification) {
        guard let result = notification.object as? SmiResult else {
            print("[dmi] sdStateChangeReceiver did not get SmiResult")
            return
        }
        self.initalSmiResult = result;
    }
    
    @objc func getSmiResult() -> SmiResult? {
        return initalSmiResult
    }
 
}
