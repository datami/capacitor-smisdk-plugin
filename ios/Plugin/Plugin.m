#import <Foundation/Foundation.h>
#import <Capacitor/Capacitor.h>

// Define the plugin using the CAP_PLUGIN Macro, and
// each method the plugin supports using the CAP_PLUGIN_METHOD macro.
CAP_PLUGIN(SmiSdkCapacitorPlugin, "SmiSdkCapacitorPlugin",
           CAP_PLUGIN_METHOD(echo, CAPPluginReturnPromise);
           CAP_PLUGIN_METHOD(setupStateChangeListener, CAPPluginReturnPromise);
           CAP_PLUGIN_METHOD(getAnalytics, CAPPluginReturnPromise);
           CAP_PLUGIN_METHOD(stopSponsoredData, CAPPluginReturnPromise);
           CAP_PLUGIN_METHOD(startSponsoredData, CAPPluginReturnPromise);
           CAP_PLUGIN_METHOD(updateUserId, CAPPluginReturnPromise);
           CAP_PLUGIN_METHOD(updateUserTag, CAPPluginReturnPromise);
           CAP_PLUGIN_METHOD(getSDAuth, CAPPluginReturnPromise);
)


