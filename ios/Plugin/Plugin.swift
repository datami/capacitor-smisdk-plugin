import Foundation
import Capacitor
import SmiSdk

/**
 * Please read the Capacitor iOS Plugin Development Guide
 * here: https://capacitor.ionicframework.com/docs/plugins/ios
 */
@objc(SmiSdkCapacitorPlugin)
public class SmiSdkCapacitorPlugin: CAPPlugin {
    
    @objc func echo(_ call: CAPPluginCall) {
        let value = call.getString("value") ?? ""
        print("[dmi]\(value)")
        call.success([
            "value": value
        ])
    }
    
    @objc func setupStateChangeListener(_ call: CAPPluginCall) {
        guard let result = SmiSdkHelper.shared.getSmiResult() else {
            print("[dmi] No inital result available")
            NotificationCenter.default.addObserver(self, selector: #selector(stateChange(notification:)), name: NSNotification.Name(rawValue: SDSTATE_CHANGE_NOTIF), object: nil)
            call.resolve()
            return
        }
        var ret = ["sdState":getSdStateString(result.sdState),
        "sdReason":getSdReasonString(result.sdReason)]
        if let carrierName = result.carrierName {
            ret["carrierName"] = carrierName
        }
        if let clientIp = result.clientIp {
            ret["clientIp"] = clientIp
        }
        self.notifyListeners("sdStateChange", data: ret)
        NotificationCenter.default.addObserver(self, selector: #selector(stateChange(notification:)), name: NSNotification.Name(rawValue: SDSTATE_CHANGE_NOTIF), object: nil)
        call.resolve()
    }
    
    
    @objc func stateChange(notification : Notification) {
        guard let result = notification.object as? SmiResult else {
            print("[dmi] sdStateChangeReceiver did not get SmiResult")
            return
        }
        var ret = ["sdState":getSdStateString(result.sdState),
        "sdReason":getSdReasonString(result.sdReason)]
        if let carrierName = result.carrierName {
            ret["carrierName"] = carrierName
        }
        if let clientIp = result.clientIp {
            ret["clientIp"] = clientIp
        }
        self.notifyListeners("sdStateChange", data: ret)
    }
    
    @objc func getAnalytics(_ call: CAPPluginCall) {
        if let analytics = SmiSdk.getAnalytics()
        {
            call.resolve([
                "fgCellularSessionTime":analytics.fgCellularSessionTime,
                "fgWifiSessionTime":analytics.fgWifiSessionTime,
                "sdDataUsage":analytics.sdDataUsage
            ])
        }
        else {
            call.reject("Unable to get Analytics")
        }
    }
    
    @objc func stopSponsoredData(_ call: CAPPluginCall) {
        SmiSdk.stopSponsorData()
        call.resolve()
    }
    
    @objc func startSponsoredData(_ call: CAPPluginCall) {
        SmiSdk.startSponsorData()
        call.resolve()
    }
    
    @objc func updateUserId(_ call: CAPPluginCall) {
        if let value = call.getString("userId")  {
            SmiSdk.updateUserId(value)
            call.resolve()
        }
        else {
            call.reject("User id not passed")
        }
    }
    
    @objc func updateUserTag(_ call: CAPPluginCall) {
        
        if let value = call.getArray("tags", String.self) {
            if(value.count > 0) {
                SmiSdk.updateTag(value)
                call.resolve()
            }
            else {
                call.reject("Tags array empty")
            }
        }
        else {
            call.reject("Tags not passed")
        }
    }
    
    @objc func getSDAuth(_ call: CAPPluginCall) {
        if let url = call.getString("url"), let apiKey = call.getString("apiKey")   {
            let result = SmiSdk.getSDAuth(apiKey, url: url, userId: "")
            var ret = ["sdState":getSdStateString(result!.sdState),
            "sdReason":getSdReasonString(result!.sdReason),
            "url":result!.url!]
            if let carrierName = result!.carrierName {
                ret["carrierName"] = carrierName
            }
            if let clientIp = result!.clientIp {
                ret["clientIp"] = clientIp
            }
            call.resolve(ret)
        }
        else {
            call.reject("Url or Api Key not passed")
        }
    }
    
    
    @objc func getSdStateString(_ sdState:SdState) -> String {
        switch sdState {
        case .SD_AVAILABLE:
            return "SD_AVAILABLE"
        case .SD_WIFI:
            return "SD_WIFI"
        case .SD_NOT_AVAILABLE:
            return "SD_NOT_AVAILABLE"
        default:
            return ""
        }
    }
    
    @objc func getSdReasonString(_ sdReason:SdReason) -> String {
        switch sdReason {
        case .SD_REASON_NONE:
            return "SD_REASON_NONE"
        case .SD_REASON_DUPLICATE_API_CALL:
            return "SD_REASON_DUPLICATE_API_CALL"
        case .SD_NOT_AVAILABLE_FOR_OPERATOR:
            return "SD_NOT_AVAILABLE_FOR_OPERATOR"
        case .SD_NOT_AVAILABLE_FOR_APPLICATION:
            return "SD_NOT_AVAILABLE_FOR_APPLICATION"
        case .SD_NOT_AVAILABLE_FOR_URL:
            return "SD_NOT_AVAILABLE_FOR_URL"
        case .SD_NOT_AVAILABLE_FOR_DEVICE_OR_USER:
            return "SD_NOT_AVAILABLE_FOR_DEVICE_OR_USER"
        case .SD_NOT_AVAILABLE_PROMOTION_EXPIRED:
            return "SD_NOT_AVAILABLE_PROMOTION_EXPIRED"
        case .SD_NOT_AVAILABLE_PROMOTION_SUSPENDED:
            return "SD_NOT_AVAILABLE_PROMOTION_SUSPENDED"
        case .SD_NOT_AVAILABLE_PROMOTION_LIMIT_EXCEEDED:
            return "SD_NOT_AVAILABLE_PROMOTION_LIMIT_EXCEEDED"
        case .SD_NOT_AVAILABLE_USER_LIMIT_EXCEEDED:
            return "SD_NOT_AVAILABLE_USER_LIMIT_EXCEEDED"
        case .SD_NOT_AVAILABLE_PROMOTION_NOT_FOUND:
            return "SD_NOT_AVAILABLE_PROMOTION_NOT_FOUND"
        case .SD_NOT_AVAILABLE_SDK_INTERNAL_ERROR:
            return "SD_NOT_AVAILABLE_SDK_INTERNAL_ERROR"
        case .SD_NOT_AVAILABLE_SDK_VERSION_NOT_SUPPORTED:
            return "SD_NOT_AVAILABLE_SDK_VERSION_NOT_SUPPORTED"
        case .SD_NOT_AVAILABLE_SDK_REGISTRATION_NOT_DONE:
            return "SD_NOT_AVAILABLE_SDK_REGISTRATION_NOT_DONE"
        case .SD_NOT_AVAILABLE_SDK_NOT_SUPPORTED_FOR_APP_OR_CARRIER:
            return "SD_NOT_AVAILABLE_SDK_NOT_SUPPORTED_FOR_APP_OR_CARRIER"
        case .SD_NOT_AVAILABLE_URAM_NO_HEADER_INJECTED:
            return "SD_NOT_AVAILABLE_URAM_NO_HEADER_INJECTED"
        case .SD_NOT_AVAILABLE_SD_NOT_SUPPORTED_IN_ROAMING:
            return "SD_NOT_AVAILABLE_SD_NOT_SUPPORTED_IN_ROAMING"
        case .SD_NOT_AVAILABLE_SD_TESTING:
            return "SD_NOT_AVAILABLE_SD_TESTING"
        case .SD_NOT_AVAILABLE_APP_API_ENABLED:
            return "SD_NOT_AVAILABLE_APP_API_ENABLED"
        case .SD_NOT_AVAILABLE_GW_CONNECTION_FAILURE:
            return "SD_NOT_AVAILABLE_GW_CONNECTION_FAILURE"
        case .SD_NOT_AVAILABLE_NO_DATA_CONNECTION:
            return "SD_NOT_AVAILABLE_NO_DATA_CONNECTION"
        case .SD_NOT_AVAILABLE_CONNECTION_TIMEOUT:
            return "SD_NOT_AVAILABLE_CONNECTION_TIMEOUT"
        case .SD_NOT_AVAILABLE_CONNECTION_LOST:
            return "SD_NOT_AVAILABLE_CONNECTION_LOST"
        case .SD_NOT_AVAILABLE_DNS_ERROR:
            return "SD_NOT_AVAILABLE_DNS_ERROR"
        case .SD_NOT_AVAILABLE_APP_TRANSPORT_SECURITY_ERROR:
            return "SD_NOT_AVAILABLE_APP_TRANSPORT_SECURITY_ERROR"
        case .SD_NOT_AVAILABLE_SD_STOP_API_CALL:
            return "SD_NOT_AVAILABLE_SD_STOP_API_CALL"
        case .SD_NOT_AVAILABLE_ZERO_BALANCE_CAMPAIGN:
            return "SD_NOT_AVAILABLE_ZERO_BALANCE_CAMPAIGN"
        case .SD_AVAILABLE_ONLY_FOR_ZERO_BALANCE_USERS:
            return "SD_AVAILABLE_ONLY_FOR_ZERO_BALANCE_USERS"
        case .SD_NOT_AVAILABLE_REASON_UNKNOWN:
            return "SD_NOT_AVAILABLE_REASON_UNKNOWN"
        default:
            return "SD_NOT_AVAILABLE_REASON_UNKNOWN"
        }
    }
    
}
