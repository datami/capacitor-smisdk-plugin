import { WebPlugin } from '@capacitor/core';
import { SmiSdkCapacitorPluginPlugin } from './definitions';

export class SmiSdkCapacitorPluginWeb extends WebPlugin implements SmiSdkCapacitorPluginPlugin {
  constructor() {
    super({
      name: 'SmiSdkCapacitorPlugin',
      platforms: ['web']
    });
  }

  async echo(options: { value: string }): Promise<{value: string}> {
    console.log('ECHO', options);
    return options;
  }
}

const SmiSdkCapacitorPlugin = new SmiSdkCapacitorPluginWeb();

export { SmiSdkCapacitorPlugin };

import { registerWebPlugin } from '@capacitor/core';
registerWebPlugin(SmiSdkCapacitorPlugin);
