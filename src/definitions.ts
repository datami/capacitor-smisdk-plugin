declare module "@capacitor/core" {
  interface PluginRegistry {
    SmiSdkCapacitorPlugin: SmiSdkCapacitorPluginPlugin;
  }
}

export interface SmiSdkCapacitorPluginPlugin {
  echo(options: { value: string }): Promise<{value: string}>;
}
