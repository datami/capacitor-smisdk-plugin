
  Pod::Spec.new do |s|
    s.name = 'CapacitorSmisdkPlugin'
    s.version = '1.2.9'
    s.summary = 'Smisdk plugin for capacitor'
    s.license = 'EULA'
    s.homepage = 'https://bitbucket.org/datami/capacitor-smisdk-plugin'
    s.author = 'Datami'
    s.source = { :git => 'https://bitbucket.org/datami/capacitor-smisdk-plugin.git', :tag => s.version.to_s }
    s.source_files = 'ios/Plugin/*.{swift,h,m,c,cc,mm,cpp}'
    s.ios.deployment_target  = '12.0'
    s.vendored_frameworks = 'ios/Plugin/SmiSdk.framework'
    s.dependency 'Capacitor'
    s.public_header_files = 'ios/Plugin/Plugin.h'
  end