package com.datami.smisdk.capacitor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import android.app.Application;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;


import com.datami.smi.SdState;
import com.datami.smi.SdStateChangeListener;
import com.datami.smi.SmiResult;
import com.datami.smi.SmiSdk;

public class DatamiApplication extends Application implements SdStateChangeListener {
    final String TAG = "[dmi] Datami";

    public static SmiResult currentSmiResult;
    public static String API_KEY;


    @Override
    public void onCreate() {
        super.onCreate();
        Context context = getApplicationContext();
        int resApiKey = context.getResources().getIdentifier("api_key", "string", context.getPackageName());
        int resSdkMess = context.getResources().getIdentifier("sdk_messaging", "string", context.getPackageName());
        int resSdkNotif = context.getResources().getIdentifier("sdk_notficiation_messaging", "string", context.getPackageName());
        int resIcFolder = context.getResources().getIdentifier("icon_folder", "string", context.getPackageName());
        int resIcName = context.getResources().getIdentifier("icon_name", "string", context.getPackageName());

        int resUserId = context.getResources().getIdentifier("smisdk_user_id", "string", context.getPackageName());
        int resExcluDomains = context.getResources().getIdentifier("smisdk_exclusive_domains", "array", context.getPackageName());
        int resUserTag = context.getResources().getIdentifier("smisdk_user_tags", "array", context.getPackageName());
        int resStartProxy = context.getResources().getIdentifier("smisdk_start_proxy", "string", context.getPackageName());

        String apiKey = context.getResources().getString(resApiKey);

        if(apiKey == null){
            apiKey = "noapikeyspecified";
        }
        API_KEY = apiKey;
//
        String useSdkMessaging = context.getResources().getString(resSdkMess);

        if(useSdkMessaging == null) {
            useSdkMessaging = "false";
        }

        String useSdkNotifMessaging = context.getResources().getString(resSdkNotif);

        if(useSdkNotifMessaging == null) {
            useSdkNotifMessaging = "false";
        }
        String iconFolder = null;
        String iconName = null;

        if(resIcFolder != 0 && resIcName !=  0) {
            iconFolder = context.getResources().getString(resIcFolder);
            iconName = context.getResources().getString(resIcName);
        }

        if(iconFolder == null) {
            iconFolder = "mipmap";
        }

        if(iconName == null) {
            iconName = "icon";
        }

        String userID = "";
        if(resUserId!=0){
            String usId = context.getResources().getString(resUserId);
            if(!TextUtils.isEmpty(usId)){
                userID = usId;
            }
        }

        List<String> exclusionDomain = null;
        if(resExcluDomains!=0){
            String[] exDomainArray = context.getResources().getStringArray(resExcluDomains);
            if(exDomainArray!=null && exDomainArray.length>0){
                exclusionDomain = Arrays.asList(exDomainArray);
            }
        }

        List<String> userTags = null;
        if(resUserTag!=0){
            try{
                String[] userTagsArray = context.getResources().getStringArray(resUserTag);
                if(userTagsArray!=null && userTagsArray.length>0){
                    userTags = Arrays.asList(userTagsArray);
                }
            }catch (Exception ex){
                ex.printStackTrace();
            }
        }

        boolean startProxy = true;
        if(resStartProxy!=0){
            try{
                String startProxyStr = context.getResources().getString(resStartProxy);
                if(!TextUtils.isEmpty(startProxyStr) && startProxyStr.equalsIgnoreCase("false")){
                    startProxy = false;
                }
            }catch (Exception ex){
                ex.printStackTrace();
            }
        }

        Log.d(TAG, "apiKey " + apiKey + " useSdkMessaging " + useSdkMessaging + " useSdkNotifMessaging " + useSdkNotifMessaging + " iconFolder " + iconFolder + " iconName " + iconName);
        Log.d(TAG, "userID: " + userID + ", exclusionDomain: " + exclusionDomain + ", userTags: " + userTags + ", startProxy: " + startProxy);

        boolean sdkMessaging = Boolean.valueOf(useSdkMessaging);
        boolean sdkNotifMessaging = Boolean.valueOf(useSdkNotifMessaging);

        Log.d(TAG, "sdkMessaging " + sdkMessaging + " sdkNotifMessaging " + sdkNotifMessaging);

        if(sdkNotifMessaging) {
            int iconId = context.getResources().getIdentifier(iconName, iconFolder, context.getPackageName());
            Log.d(TAG, "iconId " + iconId);
            SmiSdk.initSponsoredData(apiKey, context, userID, iconId, sdkMessaging, exclusionDomain, userTags, startProxy);
        }else{
            SmiSdk.initSponsoredData(apiKey, context, userID, -1, sdkMessaging, exclusionDomain, userTags, startProxy);
        }
    }

    @Override
    public void onChange(SmiResult smiResult) {
        currentSmiResult = smiResult;
        Log.d("[dmi]SDState","Current State : "+smiResult.getSdState().name());
    }
}