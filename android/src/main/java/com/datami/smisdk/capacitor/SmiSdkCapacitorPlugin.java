package com.datami.smisdk.capacitor;

import android.util.Log;

import com.datami.smi.Analytics;
import com.datami.smi.SdState;
import com.datami.smi.SdStateChangeListener;
import com.datami.smi.SmiResult;
import com.datami.smi.SmiSdk;
import com.getcapacitor.JSArray;
import com.getcapacitor.JSObject;
import com.getcapacitor.NativePlugin;
import com.getcapacitor.Plugin;
import com.getcapacitor.PluginCall;
import com.getcapacitor.PluginMethod;

import org.json.JSONException;

import java.util.List;

@NativePlugin()
public class SmiSdkCapacitorPlugin extends Plugin {

    private static SdState curSdState;

    @PluginMethod()
    public void echo(PluginCall call) {
        String value = call.getString("value");

        JSObject ret = new JSObject();
        ret.put("value", value);
        call.success(ret);
    }
    @PluginMethod()
    public void setupStateChangeListener(PluginCall call) {
        if(DatamiApplication.currentSmiResult != null) {
            JSObject ret = new JSObject();
            ret.put("sdState",DatamiApplication.currentSmiResult.getSdState().name());
            ret.put("sdReason",DatamiApplication.currentSmiResult.getSdReason().name());
            if(DatamiApplication.currentSmiResult.getCarrierName() != null && DatamiApplication.currentSmiResult.getCarrierName().length() > 1) {
                ret.put("carrierName",DatamiApplication.currentSmiResult.getCarrierName());
            }
            if(DatamiApplication.currentSmiResult.getClientIp() != null && DatamiApplication.currentSmiResult.getClientIp().length() > 1) {
                ret.put("clientIp",DatamiApplication.currentSmiResult.getClientIp());
            }
            notifyListeners("sdStateChange",ret);
        }
        SmiSdk.addSdStateChangeListener(stateChangeListener);
        call.success();
    }


    @Override
    public void load() {
        super.load();
    }

    SdStateChangeListener stateChangeListener = new SdStateChangeListener() {
        @Override
        public void onChange(SmiResult smiResult) {
            JSObject ret = new JSObject();
            ret.put("sdState",smiResult.getSdState().name());
            ret.put("sdReason",smiResult.getSdReason().name());
            if(smiResult.getCarrierName() != null && smiResult.getCarrierName().length() > 1) {
                ret.put("carrierName",smiResult.getCarrierName());
            }
            if(smiResult.getClientIp() != null && smiResult.getClientIp().length() > 1) {
                ret.put("clientIp",smiResult.getClientIp());
            }
            notifyListeners("sdStateChange",ret);
        }
    };

    @PluginMethod()
    public void getAnalytics(PluginCall call) {
        Analytics analytics = SmiSdk.getAnalytics();
        JSObject ret = new JSObject();
        ret.put("fgCellularSessionTime",analytics.getCellularSessionTime());
        ret.put("fgWifiSessionTime",analytics.getWifiSessionTime());
        ret.put("sdDataUsage",analytics.getSdDataUsage());
        call.success(ret);
    }

    @PluginMethod()
    public void stopSponsoredData(PluginCall call) {
        SmiSdk.stopSponsoredData();
        call.success();
    }

    @PluginMethod()
    public void startSponsoredData(PluginCall call) {
        try {
            SmiSdk.startSponsoredData();
            call.success();
        }
        catch (Exception e) {
            e.printStackTrace();
            call.reject(e.getMessage());
        }
    }

    @PluginMethod()
    public void updateUserId(PluginCall call) {
        String value = call.getString("userId");
        if(value != null && !value.isEmpty()) {
            SmiSdk.updateUserId(value);
            call.success();
        }
        else {
            call.reject("User id not passed");
        }
    }

    @PluginMethod()
    public void updateUserTag(PluginCall call) {
        JSArray array = call.getArray("tags", new JSArray());
        try {
            List<String> tags = array.toList();
            if(tags.size() > 0) {
                SmiSdk.updateUserTag(tags);
                call.success();
            }
            else {
                call.reject("Tags array empty");
            }
        } catch (JSONException e) {
            e.printStackTrace();
            call.reject("Tags not passed");
        }
    }


    @PluginMethod()
    public void getSDAuth(PluginCall call) {
        String url = call.getString("url");
        String apiKey = call.getString("apiKey");
        if(url.length() > 1 && apiKey.length() > 1) {
            try {
                SmiResult result = SmiSdk.getSDAuth(apiKey, this.getContext(),url,"");

                JSObject ret = new JSObject();
                ret.put("sdState",result.getSdState().name());
                ret.put("sdReason",result.getSdReason().name());
                ret.put("url",result.getUrl());
                if(result.getCarrierName() != null && result.getCarrierName().length() > 1) {
                    ret.put("carrierName",result.getCarrierName());
                }
                if(result.getClientIp() != null && result.getClientIp().length() > 1) {
                    ret.put("clientIp",result.getClientIp());
                }
                call.success(ret);
            } catch (Exception e) {
                e.printStackTrace();
                call.reject(e.getMessage());
            }
        }
        else {
            call.reject("Url or Api Key not passed");
        }
    }
}
